/*
 * GET users listing.
 */

 exports.list = function(req, res, next){
  if(req.session.user_id){
    var userList = [];
    var users = req.db.users.find().toArray(function (err, users) {
      userList = users || [];
    });
    req.db.tasks.find({completed: false}).toArray(function(error, tasks){
      if (error) return next(error);
      res.render('tasks', {
        title: 'Todo List',
        tasks: tasks || [],
        users: userList,
        csrfToken : res.locals._csrf
      });
    });
  }

  else res.redirect ("/login")
}

exports.add = function(req, res, next){
  if (!req.body || !req.body.task|| !req.body.assignee|| !req.body.deadline) return next(new Error('No data provided.'));
  req.db.tasks.save({
    author: req.session.user_name,
    assignee: req.body.assignee,
    task: req.body.task,
    createdat: new Date().toLocaleDateString(),
    deadline: new Date(req.body.deadline).toLocaleDateString(),
    completed: false
  }, function(error, task){
    if (error) return next(error);
    if (!task) return next(new Error('Failed to save.'));
    res.redirect('/tasks');
  })
};

exports.modify = function(req, res, next){
  if(req.body.task){
    req.db.tasks.update({
      task: req.db.tasks.task
    }, {$set: {
      task: req.body.task
    }},  function(error, count){
      if (error) return next(error);
    })
  }
  if(req.body.deadline){
    req.db.tasks.update({
      deadline: req.db.tasks.deadline
    }, {$set: {
      deadline: req.body.deadline
    }},  function(error, count){
      if (error) return next(error);
    })
  }  
  if(req.body.assignee){
    req.db.tasks.update({
      assignee: req.db.tasks.assignee
    }, {$set: {
      assignee: req.body.assignee
    }},  function(error, count){
      if (error) return next(error);
    })
  }
}

exports.markAllCompleted = function(req, res, next) {
  if (!req.body.all_done || req.body.all_done !== 'true') return next();
  req.db.tasks.update({
    completed: "",
    completedtime: ""
  }, {$set: {
    completed: true,
    completedtime: new Date().toLocaleDateString()
  }}, {multi: true}, function(error, count){
    if (error) return next(error);
    console.info('Marked %s task(s) completed.', count);
    res.redirect('/tasks');
  })
};

exports.completed = function(req, res, next) {
  if(req.session.user_id){
    req.db.tasks.find({completed: true}).toArray(function(error, tasks) {
      res.render('tasks_completed', {
        title: 'Completed',
        tasks: tasks || []
      });
    });
  }
  else res.redirect('/login')
}

exports.markCompleted = function(req, res, next) {
  if (!req.body.completed) return next(new Error('Param is missing'));
  req.db.tasks.updateById(req.task._id, {$set: {completed: req.body.completed === 'true',completedtime: new Date().toLocaleDateString()}}, function(error, count) {
    if (error) return next(error);
    if (count !==1) return next(new Error('Something went wrong.'));
    console.info('Marked task %s with id=%s completed.', req.task.name, req.task._id);
    res.redirect('/tasks');
  })
};

exports.del = function(req, res, next) {
  req.db.tasks.removeById(req.task._id, function(error, count) {
    if (error) return next(error);
    if (count !==1) return next(new Error('Something went wrong.'));
    console.info('Deleted task %s with id=%s completed.', req.task.name, req.task._id);
    res.send(200);
    res.redirect('/tasks');
  });
}; 

exports.del2 = function(req, res, next) {
  req.db.tasks.removeById(req.task._id, function(error, count) {
    if (error) return next(error);
    if (count !==1) return next(new Error('Something went wrong.'));
    console.info('Deleted task %s with id=%s completed.', req.task.name, req.task._id);
    res.send(200);
    res.redirect('/tasks/completed');
  });
}; 

exports.empty = function(req, res, next) {
 req.db.tasks.remove({completed: true}, function(error, count) {
  if (error) return next(error);
  if (count !==1) return next(new Error('Something went wrong.'));
  console.info('Deleted task %s with id=%s completed.', req.task.name, req.task._id);
  res.send(200);
  res.redirect('/tasks');
});
}; 