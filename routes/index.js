
/*
 * GET home page.
 */


 exports.index = function(req, res){
 	if(req.session.user_id){
 		res.render('index', {
 			title: 'Express',
 		});
 	}
 	else{
 		res.render('login', { 
 			title : "login",
 			csrfToken : res.locals._csrf
 		});
 	}
 };