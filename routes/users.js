exports.add = function(req, res, next){
  if (!req.body || !req.body.name || !req.body.password) {
    var error = new Error('Not valid params!');
    res.render('error', { title : error.message });
    res.redirect('/reg');
    return;
  }

  req.db.users.save({
    name: req.body.name,
    password: req.body.password,
  }, 
  function(error, name, password, email){
    if (error) return next(error);
    if (!name || password) {
      var error = new Error('Not saved!');
      res.render('error', { title : error.message });
      res.redirect('/reg');
      return;
    }
    res.redirect('/');
  });
};

exports.modify = function(req, res, next){
  if(req.body.name != ""){
    req.db.users.update({
      name: req.session.user_name
    }, {$set: {
      name: req.body.name
    }},  function(error, count){
      if (error) return next(error);
    })
  }
  if(req.body.password != ""){
    req.db.users.update({
      password: req.session.user_pw
    }, {$set: {
      password: req.body.password
    }},  function(error, count){
      if (error) return next(error);
    })
  }  
    
  res.redirect('/');
}

exports.list = function(req, res, next){
  req.db.users.find().toArray(function(error, user){
    if (error) return next(error);
    res.render('user', {
      title: 'Users',
      user: user || []
    });
  });
};

// exports.register = function (req, res, next) {
//   if (!req.body || !req.body.name || !req.body.password) {
//     var error = new Error('Not valid params!');
//     res.render('error', { title : error.message });
//     return;
//   };
// }
exports.profil = function(req, res, next){
  if (!req.session.user_id) {var error = new Error('Not logged in!');res.render('error', { title : error.message }); return;}
  else{
    res.render('profil', { 
      title : "Profil",
      csrfToken : res.locals._csrf,
      name: req.session.user_name,
      password: "password"
    });
  }
};


exports.login = function (req, res, next) {
  if (!req.body || !req.body.name || !req.body.password) {
    var error = new Error('Not valid params 1!');
    res.render('error', { title : error.message });
    return;
  };

  var cursor = req.db.users.find( { name : req.body.name} );

  cursor.nextObject(function(err,user){
    if (err){
      var error = new Error('Not valid params!');
      res.render('error', { title : error.message });
      return;
    }
    var validPw = user.password;
    if(req.body.password != validPw) {
      var error = new Error('Not valid pw!');
      res.render('error', { title : error.message });
      return;
    }
    req.session.user_id = user._id;
    req.session.user_name = user.name;
    req.session.user_pw = user.password;

    res.redirect('/tasks');

  });
}

