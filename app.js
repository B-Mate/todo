
/**
 * Module dependencies.
 */

 var express = require('express');
 var routes = require('./routes');
 var tasks = require('./routes/tasks');
 var users = require('./routes/users');
 var http = require('http');
 var path = require('path');
 var mongoskin = require('mongoskin');
 var hbs = require('hbs');
 var fs = require('fs');
// var Handlebars = require('handlebars');
// Register helpers
// var handlebarsLayouts = require('handlebars-layouts')(Handlebars);

// // Register partials
// Handlebars.registerPartial('layout', fs.readFileSync('views/layout.html', 'utf8'));

var db = mongoskin.db('mongodb://localhost:27017/todo?auto_reconnect', {safe:true});
var app = express();
app.use(function(req, res, next) {
  req.db = {};
  req.db.tasks = db.collection('tasks');
  req.db.users = db.collection('users');
  next();
});
app.locals.appname = 'Express.js Todo App'
// all environments

app.set('port', process.env.PORT || 3000);
// app.set('views', __dirname + '/views');
// app.set('view engine', 'jade');

app.set('view engine', 'html');
app.engine('html', hbs.__express);

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(express.session({secret: '59B93087-78BC-4EB9-993A-A61FC844F6C9'}));
app.use(express.csrf());

app.use(require('less-middleware')({ src: __dirname + '/public', compress: true }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
  res.locals._csrf = req.session._csrf;
  return next();
})

app.use(app.router);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
app.param('task_id', function(req, res, next, taskId) {
  req.db.tasks.findById(taskId, function(error, task){
    if (error) return next(error);
    if (!task) return next(new Error('Task is not found.'));
    req.task = task;
    return next();
  });
});

app.get('/', function (req, res, next) {
  if(req.session.user_id){
    res.render('index', { 
      session: req.session.user_id,
      name: req.session.user_name,
      title : "index",
      csrfToken : res.locals._csrf
    });
  }
  else{
    res.render('login', { 
      title : "login",
      csrfToken : res.locals._csrf
    });
  }
});

app.get('/tasks', tasks.list);
app.get('/tasks/empty', tasks.empty);
app.post('/tasks', tasks.markAllCompleted)
app.post('/tasks', tasks.add);
app.post('/tasks/:task_id', tasks.markCompleted);
app.get('/tasks/completed/:task_id', tasks.del2);
app.get('/tasks/completed', tasks.completed);
app.get('/tasks/:task_id', tasks.del);
  app.get('/profil/', function(req, res, next) {
      if(req.session.user_id){
    res.render('profil', { 
      title : "profil",
      csrfToken : res.locals._csrf
    });
      }
      else res.redirect('/login')
  });
  app.post('/profil', users.modify);
app.get('/login', function (req, res, next) {
  res.render('login', { 
    title : "login",
    csrfToken : res.locals._csrf
  });
});

app.get('/reg', function (req, res, next) {
  res.render('reg', { 
    title : "Sign Up TODAY!",
    csrfToken : res.locals._csrf
  });
});


app.post('/', users.login);
app.post('/reg', users.add);

app.get('/logout', function (req, res) {
  delete req.session.user_id;
  res.redirect('/login');
});     

app.all('*', function(req, res){
  res.send(404);
})
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});